# avail-lessons - Get all available lessons and possible school events from webuntis

The `lessons.py` script can be used to retrieve a list of all future lessons in the school year for a specific class and subject from a webuntis server.

I use the script to create sequence plans for the classes I teach.

Because some lesson potentially get canceled because of school events the script supports reading events from ICS files (links and files are supported) and showing each event during one of the lessons.

Example lessons starting from the current day (`2023-04-05`) using the default format:

    $ ./lessons.py --class 6D --subject NuT-Inf --show-events
    
    16 18.04 Event1
    17 25.04 Event2
    18 02.05 Event2 & Event3
    19 09.05 Event4 & Event5 & Event6
    20 16.05 Event7 & Event8
    21 23.05 Event7 & Event9
    24 13.06 Event10 & Event11
    25 20.06 Event12
    26 27.06 Event12
    27 04.07 Event13 & Event14
    28 11.07
    29 18.07
    30 25.07

You have do decide manually if an event is relevant for your planning.

## Requirements

The script requires [toml](https://pypi.org/project/toml/), [webuntis](https://pypi.org/project/webuntis/) and [icalendar](https://pypi.org/project/icalendar/) python packages and was written for python version 3.12.
A pipenv file is included in the repository to install the required dependencies in a python 3.12 virtual environment.
Run `pipenv install` to create a virtual environment containing all requirements.
Afterwards, the virtual environment can be entered by running `pipenv shell`.
Alternatively, the script can be run inside the virtual environment using `pipenv run -- ./lesson.py [options]`.

## Configuration

A simple [toml](https://toml.io/en/) configuration file is supported following the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html).
All command line options are supported in the configuration file.
Additionally, an array of ICS calendar URIs can be specified with the `icalendars` key.
A sample configuration file is included in the repository.

The probably most interesting configuration options are `output_header`, `output_footer` and `fmt` that control the format of the gathered lessons and events.

I personally use

```toml
output_header = """
Woche Datum     LP-Abschnitt Std. LP-A. Unterrichtseinheit
----- --------- ------------ ---------- ------------------------"""
fmt = "{week:>5} {date.strftime('%d.%m'):>9} {' '*12} {' '*10} {period_events}"
```

to generate a formatted simple table to use with pandoc.

To generate a latex table one could use:

```toml
output_header = '''
\begin{tabular}{l l l l l}
Woche & Datum & LP-Abschnitt & Std. LP-A. & Unterrichtseinheit \\'''
fmt = "{week} & {date.strftime('%d.%m')} & & & {period_events} \\"
output_footer = '\end{tabular}'
```

The configuration table `event_line_formats` allows to specify the format of the whole line if a certain event takes place during the lesson.

For example

```toml
[event_line_formats]
Unterrichtsschluss = '\\rowcolor{{red!30}} {line}'
```

can be used to color the latex table row in light red if a early end of class is detected.


## Usage

To obtain a list of future lessons for an example (5A) math class run `./lessons.py --class 5A --subject M`.

**Note**: the specified subject and class name must match the your webuntis conventions.

    usage: lessons.py [-h] [--school SCHOOL] [--server SERVER] [--user USER] [--password PASSWORD]
                      [--config CONFIG] [--teacher TEACHER] [--subject SUBJECT] [--class CLASS]
                      [--show-events]
    
    options:
      -h, --help           show this help message and exit
      --school SCHOOL      The shool identifier used by webuntis
      --server SERVER      The webuntis server
      --user USER          The webuntis login username
      --password PASSWORD  The webuntis login password
      --config CONFIG      Path to a configuration file
      --teacher TEACHER    The teacher abbreviation used in webuntis
      --subject SUBJECT    The subject abbreviation used in webuntis
      --class CLASS        The class name
      --show-events        Show ICS calendar events during the lessons

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
